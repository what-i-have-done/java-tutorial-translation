======================================
类和对象（Class and Objects）
======================================

定义你自己的类，包含声明 :rb:`成员变量|member variables` 、:rb:`方法|method` 、和 :rb:`构造函数|constructor` 。

你将学习到使用你的类来创建对象，并且怎用使用你创建的对象。

这节可也涵盖在其它的类中 :rb:`嵌套类|nesting class` ，和 :rb:`枚举|enumeration` 。

类（Classes）
   本节向你展示了类的 :rb:`anatomy|剖析结构` ，和怎样声明 :rb:`字段|field` 、:rb:`方法|method` 、和 :rb:`构造函数|constructor` 。

对象（Objects）
   本节涵盖了创建和使用对象。你将学习到怎样初始化一个对象，并且，一旦被初始化，怎样使用 :code:`.` 操作符（dot） :rb:`访问|access` 对象的 :rb:`实例对象和方法|instance variables and methods` 。

更多的在类上（More on Classes）
   本节涵盖相关类更多的方面，依赖于使用 :rb:`对象引用|object reference` 和 `.` 操作符，包含有：从方法中的 :rb:`返回值|returning value` 、 :code:`this` 关键字 、:rb:`类成员|class member` vs. :rb:`实例成员|instance member` 和 :rb:`访问控制|access control` 。

嵌套类（Nested Classes）
   本节涵盖了 :rb:`静态嵌套类|static nested class` 、 :rb:`内部类|inner class` 、 :rb:`匿名内部类|anonymous inner class` 、 :rb:`本地类|local class` 、 :rb:`lambda 表达式|lambda expression` 。还讨论了何时使用哪种方法。

枚举类型（Enum Types）
   本节涵盖了 :rb:`枚举|enumeration` 、允许你定义和使用 :rb:`常量集|sets of constants` 的 :rb:`专用类|specialized class` 。

1 类（Classes）
===================

1.1 声明类（Declaring Classes）
-------------------------------

.. code:: java

   class MyClass {
      //field, constructor, and
      // method declarations
   }

这个一个 :term:`类声明|class declaration` 。* :rb:`类主体|class body` * （花括号之间的区域）包含了为从类创建的对象的 :rb:`生命周期|life cycle` 提供的所有代码：用于初始化新对象的 **构造函数** 、为提供类和它的对象的 :rb:`状态|state` 的 **字段** 、和实现类和它的对象的 :rb:`行为|behavior` 的 **方法** 。

以上仅包含必要的类声明的那些组件。你也可以在 **类声明的开头** 提供该类的更多信息，例其 :rb:`超类|superclass` 的名称，是否 :rb:`实现|implement` 什么 :rb:`接口|interface` 等。

.. code:: java

   class MyClass extends MySuperClass implements YourInterface {
       // field, constructor, and
       // method declarations
   }

通常，类声明可以 :rb:`按顺序|in order` 包含这些组件：

1. :rb:`修饰符|modifier` 如 :code:`public` 、 :code:`private` 和其它一些将在之后遇到的。
2. :rb:`类名|class name` ，首字母大写（约定）
3. :rb:`父类名称|the name of the class's parent` （ :rb:`超类|superclass` ） 如果有的话，:code:`extend` 关键字会在它前面。 **一个类仅能 :term:`扩展|extend` 一个父类。**
4. 由类 :rb:`实现|implement` 的以逗号分隔的 :rb:`接口|interface` 列表，如果有的话， :code:`implements` 关键字会在它的前面。 **一个类可以 :term:`实现|implement` 多个接口。**
5. 被花括号 :code:`{}` 包围起来的 :rb:`类主体|class body` 。

1.2 声明成员变量（Declaring Member Variables）
----------------------------------------------

有几种 :rb:`变量|variable` ：

- 在类中的成员变量 -- :term:`字段|field`
- 在方法或者代码块中的变量 -- :term:`本地变量|local variable`
- 在方法声明中的变量 -- :term:`参数|parameter`

:rb:`字段声明|field declartion` 有 3 部分组成，按顺序是：

1. 0 个或多个修饰符，例如 :code:`public` 或 :code:`private`
2. 字段类型
3. 字段名称

1.2.1 访问修饰符（Access Modifiers）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

使用的 **第一个（最左侧）** 修饰符让你控制其它类可以访问到 :rb:`成员字段|member field` 。

- :code:`public` 修饰符 -- 所有的类都可以访问该字段
- :code:`private` 修饰符 -- 该字段仅能在自己的类中访问

1.2.2 类型（Types）
~~~~~~~~~~~~~~~~~~~

所有的变量 **必须** 有一个类型。可以使用 :rb:`原始类型|primitive type` 。或者使用 :rb:`引用类型|reference type` 。

1.2.3 变量名（Variable Names）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

所有变量，无论是字段、本地变量、或者参数都遵循 `Variables-Naming`_ 中介绍的相同命名规则和约定。

**注意：** 方法和类名使用相同的命名规则和约定，除了

- 类名的第一个字母应该大写，和
- 方法名中第一个（或唯一）单词应该是一个动词。

1.3 定义方法（Defining Methods）
--------------------------------

.. code:: java

   public double calculateAnswer(double wingSpan, int numberOfEngines,
                              double length, double grossTons) {
    //do the calculation here
   }

:rb:`方法声明|method declaration` 中唯一必需的元素是方法的 :rb:`返回类型|return type` 、名称、一对括号 :code:`()` 、花括号之间的主体 :code:`{}` 。

更一般地，方法声明有 6 个组件，按顺序如下：

1. :rb:`修饰符|modifier` -- 如 :code:`public` 、 :code:`private` 。
2. :rb:`返回类型|return type` -- 由方法返回的值的数据类型，或者 :code:`void` 如果方法没有返回一个值。
3. :rb:`方法名|method name`
4. 在括号中的 :rb:`参数列表|parameter list` -- 以 :rb:`逗号分隔|comma-delimited` 的输入参数列表，数据类型在前，括在括号里 :code:`()` 。如果没有参数，就必须使用空括号。
5. :rb:`异常列表|exception list`
6. :rb:`方法体|method body` 括在花括号 :code:`{}` 之间。

.. important::
   方法声明的 2 个组件组成了 :term:`方法签名|method signature` -- :rb:`方法名|method's name` 和 :rb:`参数类型|parameter type`

1.3.1 命名一个方法（Name a Method）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

按照惯例，方法名应该是

1.3.2 重载方法（Overloading Methods）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Java 编程语言支持 :term:`重载|overloading` 方法。Java 可以区分具有不同 :term:`方法签名|method signature` 的方法。

1.4 提供构造函数为你的类（Providing Constructors for Your Classes）
-----------------------------------------------------------------

类包含 :rb:`构造函数|constructor` ，被 :rb:`调用|invoke` 以从 :rb:`类蓝图|class blueprint` 中创建对象。构造函数声明和方法声明看起来很想，除了构造函数 **使用类名** 和 **没有返回类型** 。

正如方法，Java 平台根据列表中的 **参数数量和类型** 来区分构造函数。

编译器自动为没有构造函数的任何类提供一个 :rb:`无参数|no-argument` 的 :rb:`默认构造函数|default constructor` 。这个默认构造函数将调用超类的无构造函数。在这样的情况下，如果超类没有无参构造函数，编译器将会报错。如果没有 :rb:`显示的|explicit` 超类，那么它有一个 :rb:`隐式的|implicit` 超类 :code:`Object` ，它有一个无参的构造函数。

你可以使用 :rb:`访问修饰符|access modifier` 在构造函数声明中来控制哪个其它类可以调用这个构造函数。

1.5 传递信息给方法或构造函数（Passing information to a Method or a Constructor）
------------------------------------------------------------------------------

方法或构造函数声明 :rb:`声明了|declare` 那个方法或构造函数 :rb:`参数|argument` 的 :rb:`数量|number` 和 :rb:`类型|type` 。

.. note::
   :term:`参数|parameter` 是指方法声明中的变量列表；:term:`参数|argument` 是当调用方法时传递的实际值。在你调用方法是，使用的 argument 必须和声明中的 parameter 在 **类型和顺序** 上想匹配。

1.5.1 参数类型（Parameter Types）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

你可以将任何的数据类型用于方法和构造函数的 :rb:`参数|parameter` 。包括 :rb:`原始数据类型|primitive data type` 和 :rb:`引用数据类型|reference data type` 。

1.5.2 任意数量的参数（Arbitrary Number of Arguments）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

你可以使用一个叫 :term:`可变参数|varargs` 的 :construct:`构造物|construct` 来传递 :rb:`任意数量|arbitrary number` 的值给方法。

使用可变参数，将 :rb:`省略号|ellipsis` （ 三个点 :code:`...` ）放在 **最后一个参数的类型** 后面，然后一个空格，加上参数名。这个方法可以使用荣任何数量的参数来调用，包含 :rb:`没有参数|none` 。

1.5.3 参数名（Parameter Names）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

向方法或构造函数声明参数时，为该参数提供一个 :rb:`名称|name` 。这个名字在 :rb:`方法体|method body` 内用于引用 :rb:`传递的|passed-in` 参数。

参数名称在它的 :rb:`范围|scope` 内必须是 :rb:`唯一的|unique` 。 它不能和相同方法或构造函数的另一个参数名相同，也不能和方法或构造函数内的本地变量名称相同。

参数可以与类中某个字段具有相同的名称。如果是这样的情况，则称该参数 :term:`遮罩|shadow` 这个字段。

1.5.4 传递原始数据类型参数（Passing Primitive Data Type Arguments）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:rb:`原始参数|primitvie argument` （如 int 或 double） :rb:`按值|by value` 传递给方法。这意味着参数值的任何改变 **仅存在** 在这个方法范围内。但方法返回时，参数会 :rb:`消失|gone` ，对它们的任何更改都将丢失。

.. code:: java

   public class PassPrimitiveByValue {
       public static void main(String[] args) {
           int x = 3;
           // invoke passMethod() with 
           // x as argument
           passMethod(x);
           // print x to see if its 
           // value has changed
           System.out.println("After invoking passMethod, x = " + x);
       }
       // change parameter in passMethod()
       public static void passMethod(int p) {
           p = 10;
       }
   }

1.5.5 传递引用数据类型参数（Passing Reference Data Type Arguments）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:rb:`引用数据类型参数|reference data type parameter` 也 :rb:`按值|by value` 传递给方法。这意味着 :rb:`passed-in reference|传递的引用`  任然 :rb:`引用着|reference` 之前的对象。 **但是** ，如果对象字段的值具有合适的 :rb:`访问等级|access level` ，那么可以在方法中改变它们的值。

2 对象（Objects）
=================

典型的 Java 程序会创建对象，通过 :rb:`调用方法|invoke method` 实现 :rb:`交互|interact` 。通过这些 :rb:`对象交互|object interaction` ，程序可以 :rb:`做|carry out` 各式各样的任务，例如实现 :abbr:`GUI|graphical user interface` 、运行动画、或通过网络发送和接收信息。一旦一个对象完成工作（为了这工作，它被创建），它的资源将被回收，循环供其它对象使用。

2.1 创建对象（Creating Objects）
--------------------------------

正如你知道的，类为对象创建提供 :rb:`蓝图|blueprint` ；你从类中创建对象。

.. raw:: html

   <pre>
   <b>Point originOne</b> = new Point(23, 94);
   <b>Rectangle rectOne</b> = new Rectangle(originOne, 100, 200);
   <b>Rectangle rectTwo</b> = new Rectangle(50, 100);
   </pre>

有 3 个部分：

1. :rbbd:`声明|Declartion` ：
2. :rbbd:`实例化|Instantiation` ：关键字 :code:`new` 是一个创建对象的 Java :rb:`操作符|operator` 。
3. :rbbd:`初始化|Initialization` ： :code:`new` 操作符之后跟随着一个构造函数调用，它初始化这个新对象。

2.1.1 声明一个变量来引用一个对象（Declaring a Variable to Refer to an Object）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

之前，你学习到声明一个变量，你可以这样写：

.. code::

   type name;

这告知编译器你将使用 *name* 来引用类型为 *type* 的数据。对于 :rb:`primitive varibale|原始数据类型变量` ，这个声明也为变量 :rb:`保留|reserve` 合适数量的 :rb:`内存|memory` 。

你也可以声明一个 :rb:`引用变量|referencce variable` 。

.. code:: java

   Point originOne;

如果你想这样声明 :code:`originOne` ，它的值将是不确定的，直到实际创建一个对象并 :rb:`赋值|assign` 给它。简单地声明一个引用变量并不创建对象。对于这个，你需要使用 :code:`new` 操作符。你必须在使用它之前为 :code:`originOne` 赋值一个对象，否则你将得到一个 :rb:`编译错误|compiler error` 。

这样状态的变量（当前不引用任何对象）可以如下所示：

.. image:: assets/pics/objects-null.gif
   :align: center

2.1.2 实例化一个类（Instantiating a Class）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:code:`new` 操作符 :rb:`实例化|instantiate` 一个类，通过为一个新对象 :rb:`分配|allocate` 内存并返回那个内存的 :rb:`引用|reference` 。:code:`new` 操作符也 :rb:`调用|invoke` 对象的构造函数。

.. important::
   「 :rb:`实例化一个类|instantiating a class` 」和「 :rb:`创建一个对象|creating an object` 」具有相同意思。当你创建一个对象，你就创建一个类的「 :rb:`实例|instance` 」，因此「 :rb:`实例化|instantiating` 」一个类。

:code:`new` 操作符需要 **一个** :rb:`后缀参数|postfix argument` ：一个构造函数的调用。构造函数的名称提供要实例化类的名称。

:code:`new` 操作符返回一个它创建对象的引用。这个引用通常分配个适当类型的变量。像这样：

.. code:: java

   Point originOne = new Point(23, 94);

由 :code:`new` 操作符返回的引用并不是不得不要赋值给一个变量。它也可以之间使用在 :rb:`表达式|expression` 中。

.. code:: java

   int height  = new Rectangle().height;

2.1.3 初始化一个对象（Initializing an Object）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

所有的类都至少有一个 :rb:`构造器|constructor` 。如果一个类没有明确声明任何构造器的话，Java 编译器自动提供一个 :rb:`无参构造器|no-argument constructor` ，被称作 :term:`默认构造器|default constructor` 。默认构造器调用父类的无参构造器，或者 :code:`Object` 构造器（如果类没有其它父类）。如果父类没有构造器（ :code:`Ojbect` 有一个），编译器将 :rb:`拒绝|reject` 这个程序。

2.2 使用对象（Using Objects）
-----------------------------

2.2.1 引用一个对象字段（Referencing an Object's Fields）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

对象字段由 它们的 :rb:`名字|name` 来访问。你必须使用 :rb:`明确的|unambiguous` 名字。

**在它自己类中** ，可以使用简单的名字为了字段。

**在对象类的外部** 的代码必须使用 :rb:`对象引用或表达式|object reference or expression` ，跟着 :rb:`点|dot` （ :code:`.` ）操作符，加上一个简单的字段名。

.. code:: java

   objectReference.fieldName;

2.2.2 调用一个对象方法（Calling an Object's Methods）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

你还可以使用 :rb:`对象引用|object reference` 来调用对象方法。将带有一个插入点操作符（ :code:`.` ）的方法的简单名称附加到对象引用中。另外，在括号内为方法提供参数。如果方法不需要参数，请使用空括号。

.. code:: java

   objectReference.methodName(argumentList);
   // 或者，无参数调用方法
   objectReference.methodName();

.. important::
   :rb:`在特定对象上调用方法|invoking a method on a particular object` 与 :rb:`向那个对象发送消息|sending a message to that object` 是相同的。

2.2.3 垃圾收集器（The Garbage Collector）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

一些 :rb:`面向对象语言|object-oriented language` 需要你保持跟踪你创建的所有对象，并且在它们不在需要的时候你明确地 :rb:`销毁|destory` 它们。:rb:`明确地管理内存|manage memory explicity` 是乏味并且 :rb:`容易出错|error-prone` 的。Java 平台运行你根据需要创建人员数量的对象（当然，有限制，由你的系统可以处理决定），并且不必担心销毁它们。:rb:`Java 运行环境|Java runtime environment` 在确定对象不在使用时删除它们。这个过程被称作 :term:`垃圾收集|garbage collection` 。

当不再有引用引用到那个对象时，那么对象对于垃圾收集是 :rb:`合格的|eligible` 。当超出变量 :rb:`范围|scope` 时，通常会 :rb:`删除|drop` 变量上 :rb:`持有的|hold` 引用，或者可以通过给变量设置一个特殊的值 :code:`null` 来显示地删除变量。 **记住** 程序可以对同一个对象进行多次引用；对象必须删除所有的引用才对垃圾回收器是合格的。

Java 运行环境具有垃圾收集器，可以 :rb:`定期|periodically` :rb:`释放|free` 不再引用的对象使用的内存。The garbage collector does its job automatically when it determines that the time is right.


3 更多的在类上（More on Classes）
=================================

这节课程覆盖类更多的方法，依赖于使用 :rb:`对象引用|object reference` 和 :code:`.` 操作符：

- Returning values from methods.
- The this keyword.
- Class vs. instance members.
- Access control.

3.1 返回值从方法中（Returning a Value from a Method）
-----------------------------------------------------

方法返回到调用它的代码，当它：

- 完成方法中所有的语句
- 到达一个 :code:`return` 语句，或者
- :rb:`抛出一个异常|throw an exception`

以先发生的为准。

声明方法的 :rb:`返回类型|return type` 在它的 :rb:`方法声明|method declaration` 中。在 :rb:`方法体|the body of method` 中，使用 :code:`return` 语句来返回值。

任何被声明为 :code:`void` 的方法不返回一个值。它不需要包含一个 :code:`return` 语句，但它也可以这么做。在这种情况下，可以使用 :code:`return` 语句来 :rb:`分支出|branch out of` :rb:`控制流程块|control flow block` 并退出这个方法。可以简单地使用像这样：

.. code:: java

   return;

如果你尝试从一个声明为 :code:`void` 的方法中返回一个值，你将得到一个 :rb:`编译错误|complier error` 。

任何没有声明为 :code:`void` 的方法必须包含一个 :code:`return` 语句，伴随着一个相应的 :rb:`返回值|return value` ，像这样：

.. code:: java

   return returnVaule;

返回值的数据类型必须匹配方法声明中的返回类型。

3.1.2 返回一个类或接口（Return a Class or Interface）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

当方法使用 :rb:`类名|class name` 作为它 :rb:`返回类型|return type` ， :rb:`返回对象的类型的类|the class of the type of the returned object` 必须是返回类型的 :rb:`子类|subclass` 或 :rb:`确切的类|the exact class` 。

你可以 :rb:`覆盖|override` 一个方法，并且定义它来返回原始方法的子类。

这种技术，被称为 :term:`协变返回类型|covariant return type` ，意味着允许返回类型在与子类同方向上 :rb:`变化|vary`。

3.2 使用 this 关键值（Using the this Keyword）
----------------------------------------------

在 :rb:`实例方法|instance method` 或者 :rb:`构造器|constructor` 中， :code:`this` 是对 :term:`当前对象|current object` （正在调用方法或者构造器的对象） 的引用。

3.2.1 与字段使用 this（Using this with a Field）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

使用 :code:`this` 关键字常见的理由是因为方法或构造器 :rb:`隐藏了|shadow` 字段。

.. code:: java

   public class Point {
       public int x = 0;
       public int y = 0;
        
       //constructor
       public Point(int x, int y) {
           this.x = x;
           this.y = y;
       }
   }

构造器的每个参数都会隐藏一个对象字段 -- 在构造器内部 :code:`x` 是构造器第一个参数的 :rb:`本地副本|local copy` 。为了引用到 :code:`Ponit` 的字段 :code:`x` ，构造器必须使用 :code:`this.x` 。

3.2.2 与构造器使用 this（Using this with a Constructor）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

在构造器内部，也可以使用 :code:`this` 关键字来调用相同类中的另一个构造器。 这样做被称作 :term:`显示构造函数调用|explicit constructor invocation` 。

如果存在，另一个构造函数调用必须在构造函数中的 **第一行** 。

3.3 控制对类成员的访问（Controlling Access to Members of a Class）
------------------------------------------------------------------

:rb:`访问级别修饰符|access level modifier` 决定其它类是否可以使用特定字段或者调用特定方法。有 2 个级别的访问控制：

- 在 :rb:`顶层|top level` -- :code:`public` ，或 :term:`包级私有|package-private`
- 在 :rb:`成员层|member level` -- :code:`public` ， :code:`private` ， :code:`protected` ， 或者 :term:`包级私有|package-private`

+-------------------+-----------+-------------+--------------+-----------+
| **Modifier**      | **Class** | **Package** | **Subclass** | **World** |
+===================+===========+=============+==============+===========+
| :code:`public`    |   Y       |    Y        |     Y        |   Y       |
+-------------------+-----------+-------------+--------------+-----------+
| :code:`protected` |   Y       |    Y        |     Y        |   N       |
+-------------------+-----------+-------------+--------------+-----------+
| *no modifier*     |   Y       |    Y        |     N        |   N       |
+-------------------+-----------+-------------+--------------+-----------+
| :code:`private`   |   Y       |    N        |     N        |   N       |
+-------------------+-----------+-------------+--------------+-----------+

.. note:: **选择访问级别小贴士：**

   如果其它程序员使用你的类，你想确保不会发生 :rb:`滥用|misuse` 错误。访问级别可以帮助你做到这些：

   - 使用对特等成员有意义的 **最严格的** 访问级别。使用 :code:`private` ，除非你有很好的理由不使用它。
   - 避免使用 :code:`public` 字段，除 :rb:`常量|constant` 外。Public fields tend to link you to a particular implementation and limit your flexibility in changing your code.

3.4 理解类成员（Understanding Class Members）
---------------------------------------------

本节，我们讨论使用 :code:`static` 关键字来创建 :rb:`属于类|belong to the class` 的字段和方法，而不是属于 :rb:`类的一个实例|an instance of the class` 的。

3.4.1 类变量（Class Variables）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

当从同一个 :rb:`类蓝图|class blueprint` 创建许多对象时，它们每一个都有自己不同的 :term:`实例变量|instance variable` 副本。每个对象对于这些变量都有自己的值，存储在不同的内存位置。

有时，你希望拥有所有对象 :rb:`共有|common` 的变量。这个用 :code:`static` 修饰符来完成。在字段声明中有 :code:`static` 修饰符的字段被称作 :term:`静态字段|static field` 或者 :term:`类变量|class variable` 。它们关联到类，而不是任何的对象。每个类的实例都共享一个类变量，在一个固定的内存中的位置。任何对象都可以改变类变量的值，但是类变量也可以在不创建类实例下 :rb:`操作|manipulate` 。

3.4.2 类方法（Class Methods）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Java 编程语言支持 :rb:`静态方法|static method` 以及 :rb:`静态变量|static variable` 。静态方法，在它们的声明中有 :code:`static` 修饰符，应该使用 :rb:`类名|class name` 调用，而不需要创建 :rb:`类的实例|an instance of the class` 。

.. code:: java

   ClassName.methodName(args)

.. note::
   你也可以用 :rb:`对象引用|object reference` 引用静态方法，像这样：

   .. code:: java

      instanceName.methodName(args)

   但是这并不鼓励，因为这使得它们的类方法不清晰。

静态方法常见的使用是为了访问静态字段。

并非所有的实例和类的变量和方法的组合都是允许的：

- 实例方法可以直接访问实例变量和实例方法。
- 实例方法可以直接访问类变量和类方法。
- 类方法可以直接访问类变量和类方法
- 类方法 **不可以** 直接访问实例变量和实例方法 -- 它们必须使用对象引用。同时，类方法不可以使用 :code:`this` 关键字，因为没有实例给 :code:`this` 来引用。

3.4.3 常量（Constants）
~~~~~~~~~~~~~~~~~~~~~~~

:code:`static` 修饰符同 :code:`final` 修饰符组合被用来定义 :code:`常量|constant` 。 :code:`final` 修饰符 :rb:`表示|indicate` 这个字段的值不参与改变。

.. code:: java

   static final double PI = 3.141592653589793;

以这种方法定义的常量不能 :rb:`重新赋值|reassign` ，并且如果你的程序尝试这样做的化，那么这是一个 :rb:`编译时错误|compile-time error` 。按惯例，常量值的名称拼写为 **大写字母** ，如果由多个单词组成，那么单词用下划线（ :code:`_` ）分隔。

.. note::
    If a primitive type or a string is defined as a constant and the value is known at compile time, the compiler replaces the constant name everywhere in the code with its value. This is called a compile-time constant. If the value of the constant in the outside world changes (for example, if it is legislated that pi actually should be 3.975), you will need to recompile any classes that use this constant to get the current value. 

3.5 初始化字段（Initializing Fields）
-------------------------------------

正如你所见到的，通常可以在字段声明中为字段提供 :rb:`初始值|initial value` 。

实例化对象可以在构造函数中初始化，在其中可以使用 :rb:`错误处理|error handling` 或者其它逻辑。为了给 **类变量** 提供相同的能力，Java 编程语言包含了 :term:`静态初始化块|static initialization block` 。

.. note::
   声明字段并不必要在类声明之前，虽然这是常见的做法。仅仅在使用之前声明并初始化是必要的。

:term:`静态初始化块|static initialization block` 是用花括号 :code:`{}` 括起来的常规代码块，由 :code:`static` 关键字在前。

.. code::

   static {
    // whatever code is needed for initialization goes here
   }

一个类可以由任意数量的静态初始化块，它们可以出现在类主体中任何位置。 :rb:`运行时系统|runtime system` 保证 **按照它们在源代码出现的顺序调用初始化块** 。

还有静态初始化块的替代方法 -- 可以写 :rb:`私有静态方法|private static method` ：

私有静态初始化方法的优势是你可有 :rb:`重用|resue` 如果你需要重新初始化类变量的话。

3.5.1 初始化实例成员（Initializing Instance Members）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

通常，你可以放置代码在构造函数中来初始化实例变量。使用构造函数来初始化实例变量有 2 个备选方法： :rb:`初始化块|initializer block` 和 :rb:`final 方法|final method` 。

实例变量的初始块看起来想静态初始化块，但是没有 :code:`static` 关键字。

.. code:: java

   {
    // whatever code is needed for initialization goes here
   }

Java 编译器 :rb:`复制|copy` 初始化块进每一个构造函数，因此，该方法可用于多个构造函数之间共享代码块。

:term:`final 方法|final method` 不能在子类中被 :rb:`覆盖|override` 。

如果子类想要 :rb:`重用|resue` 初始化方法，这个特别的有用。这个方法是 :code:`final` ，因为在实例初始化期间调用 :rb:`非最终方法|non-final method` 可能引起问题。

3.6 总结创建和使用类和对象（Summary of Creating and Using Classes and Objects）
-------------------------------------------------------------------------------

类声明为类 :rb:`命名|name` 并将 :rb:`类主体|class body` 括在大括号（{}）中。 :rb:`类名|class name` 前面可以放 :rb:`修饰符|modifier` 。类主体包含为类的 :rb:`字段|field` 、 :rb:`方法|method` 、 :rb:`构造函数|constructor` 。类使用字段来包含 :rb:`状态|state` 信息，用方法实现 :rb:`行为|behavior` 。初始化类实例的构造函数使用类的名字，看起来想没有返回类型的方法。

控制到类和成员访问方法是相同的：用过使用 :rb:`访问修饰符|access modifier` （如 :code:`public` ）在它们声明中。

你可以通过在成员声明中使用 :code:`static` 关键字来指定 :rb:`类变量|class variable` 或者 :rb:`类方法|class method` 。没有以 :code:`static` 声明的成员是 :rb:`隐性的|implicitly` 实例成员。类变量由类的所有实例共享，可以通过 :rb:`类名|class name` 或者一个 :rb:`实例引用|instance reference` 来访问。类的实例 :rb:`得到|get` 每个实例变量的它们自己的 :rb:`副本|copy` ，并且必须通过实例引用来访问。

使用 :code:`new` 操作符和构造函数从类中创建对象。 :code:`new` 操作符返回它创建的对象的引用。你可以 :rb:`赋值|assign` 引用给变量或者直接使用它。

**在声明实例变量和方法的类的外部** 访问它们可以通过一个 :rb:`合格的名称|qualified name` 来引用。实例变量的合格名称看起来像这样：

.. code:: java

   objectReference.variableName

实例方法的合格名称：

.. code:: java

   // 有参数
   objectReference.methodName(argumentList)

   // 或者，无参数
   objectReference.methodName()

:rb:`垃圾回收|garbage collector` 自动清理 :rb:`不使用的|unused` 对象。如果程序不再 :rb:`保留|hold` 引用在它上，那么对象是不使用的。你可以 :rb:`显式地|explicitly` :rb:`删除|drop` 引用通过设置 **保留有引用的变量** 为 :code:`null` 。


