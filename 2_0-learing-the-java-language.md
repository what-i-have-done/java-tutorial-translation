# 学习 Java 语言

## 面向对象编程概念

教面向对象（object-oriented）编程下的核心内容：对象，消息，类和继承（objects, messages, classes, and interfaces）。

## 语言基础

描述语言的传统特征，包含：变量、数组、数据类型、操作符、和控制流（variables, arrays, data types, operators, and  control flow）。

## 类和对象

描述怎样编写创建对象的类，怎样创建和使用对象。

## 注释（Annotations）

是一种元数据（metadata）的形式，提供信息给编译器（compiler）。这节课描述在哪和怎样在程序中有效地使用注释。

## 接口和继承（Interfaces and Inheritance）

描述接口（interface）-- 他们是什么，为什么你需要写一个，以及怎样写。这节课也描述从另一个类派生（derive）一个类的方法。也就是子类（subclass）怎样从超类（superclass）继承字段和方法（fields and method）。你将学习到所有的类都是从 Object 类派生出的，并怎样修改子类从超类中继承的方法。

## 数（Numbers）和字符串（Strings）

描述怎样使用 Number 和 String 对象。这节课也展示怎样格式化输出。

## 泛型（Generics）

Java 编程语言一个强大特性。它们提升代码的类型安全，使得在编译时更多的 Bugs 可以被侦测到。

## 包（Packages）

Java 编程语言帮助你组织和结构你的类和它们彼此之间的关系。
