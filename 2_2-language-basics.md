# 语言基础

- 变量（variable）

    你已近学习了对象存在它们的状态在字段中。但是，Java 编程语言也是使用「变量（variable）」这个术语。本节讨论这个之间关系，加上变量命名规则和约定，基本数据类型（primitive types, character strings, and  arrays），默认值，和字面量（literal）。

- 操作符（operator）

    本节描述 Java 编程语言的操作符。它首先介绍最常用的操作符，之后是不常用的操作符。每个讨论包含可以编译和运行的代码示例。

- 表达式、语句和语句块（expression, statement, block）

    操作符可用于构建可以计算值的表达式；表达式是语句的核心组件；语句可以分组成语句块。本节讨论表达式、语句和语句块，使用你已经看过的代码示例。

- 控制流语句（control flow statement）

    本节描述 Java 编程语言支持的控制流语句。它涵盖了决策、循环和分支语句，使你的程序能够有条件地执行特定的代码块。（decisions-making, looping, and branching statements）

## 1 变量

Java 编程语言定义了以下几种 *变量（variable）*：

- **实例变量（非静态字段 / 非静态域） [Instance Variable (Non-Static Fields)]** 技术上来说，对象存储其各个状态在「非静态字段」中，即字段没有用 `static` 关键字声明。非静态字段也被称为 *实例变量（instance variables）*，因为它们的值对于类（对于对象，换句话说）的每一个 *实例（instance）* 都是唯一的。
- **类变量（静态字段 / 静态域） [Class Variables (Static Fields)]** *类变量（class variable）* 是任何用 `static` 修饰符声明的字段。它告诉编译器（complier）这个变量只有一个副本存在，无论这个类被实例化多少次。此外，可以添加 `final` 关键字以指示 *静态字段* 将永远不会改变。
- **本地变量 / 局部变量（Local Variables）** 与对象如何在字段中存储其状态类似，方法（method）经常存储它的临时状态在本地变量 *（local variable）* 中。声明一个本地变量的语法同声明一个字段的语法类似。没有特殊的关键字将变量指定为本地（local）;该决定完全来自声明变量的位置 -- 它位于方法的开括号和闭括号之间。这样，本地变量仅对声明它们的方法可见（visiable）；它们不可以从其它类那里获取。
- **参数（Parameters）** `main` 方法的签名（signature）是 `public static void main(String[] args)`。这里的 `args` 变量即是这个方法的参数。要记住的重要一点是参数总是被归类为「变量」而不是「字段」。这也适用于我们的其它参数接受结构（paramter-accepting constructs），例如结构体和异常处理器（constructors and exception handlers）。

类型的字段、方法和嵌套类型统称为其 *成员（members）*。

#### 命名（naming）<a name="naming" />

命名变量的规则和约定总结如下：

- 变量名是大写敏感的（case-sensitive）。
- 子序列字符可以说字母、数字、`$` 符号、或者下划线字符（`_`）。当为你的变量选择名字时，使用** 完整的单词（full words）** 替代缩写。这样做是你的代码更容易阅读和理解。在大多数情况下，这也将使你的代码自成文档（self-documenting）。也要记住你选择的名字千万不要是关键字或者保留字（keyword or reserved world）。
- 字母大小写约定：
    + 一个单词 --> 全部小写字母
    + 多个单词 --> 大写子序列单词的首字母
    + 存储常量值（constant value） --> 大写全部字母，并用下划线（`_`）分隔子序列单词。**注：** 通常，下划线（`_`）从未用在其它地方

### 1.1 原始数据类型（Primitive Data Types）

Java 编程语言是静态类型的（statically-typed），这意味着所有的变量 **在使用之前** 必须先声明（declare）。

变量的数据类型确定它可能包含的 **值**，以及可能对其执行的 **操作**。Java 编程语言支持 8 种 *原始数据类型（primitive data type）*。原始数据类型由语言预定义，并由保留关键字（reserved keyword）命名。原始值（primitive value）不同其它原始值共享状态。

| 数据类型  | 占用位数（Bit） | 值的类型               | 范围                                                               | 备注 |
| --------  | --------        | --------               | ----                                                               | ---- |
| `byte`    | 8               | 二进制补码整数         | 有符号位：-128 <= byte <= 127                                      |      |
| `short`   | 16              | 二进制补码整数         | 有符号位：-32768 <= short <= 32767                                 |      |
| `int`     | 32              | 二进制补码整数         | 有符号位：-2^31 <= int < 2^31 -1 <br> 无符号位：0 <= int <= 2^32   |      |
| `long`    | 64              | 二进制补码整数         | 有符号位：-2^63 <= long < 2^63 -1 <br> 无符号位：0 <= long <= 2^64 |      |
| `float`   | 32              | 单精度 IEEE 754 浮点数 | 暂不讨论                                                           |      |
| `double`  | 64              | 双精度 IEEE 754 浮点数 | 暂不讨论                                                           |      |
| `boolean` | 未定义[^1]      | -                      | 仅有 `true` 和 `false`                                             |      |
| `char`    | 16              | Unicode 字符           | `'\u0000'` (or `0`) <= char <= `'\uffff'` (or `65535`)             |      |

有符号位（signed），无符号位（unsinged），二进制补码整数（two's complement integer），浮点数（floating point），单精度（single-precision），双精度（double-precision）

**注：**

1. 可以使用 `byte`，`short`，和 `float` 在大型数组（array）中来 **节省内存**，在节省内存实际很重要的地方。
2. 千万不应该使用 `float` 和 `double` 在 **精确值（precise value）** 中，如货币（currency）。对于这个，需要使用 `java.math.BigDecimal` 类代替。

另 Java 编程语言为 **字符串（character string）** 提供特别的支持通过 `java.lang.String` 类。将字符串括在 **双引号（`"`）** 内将自动创建一个新的 `String` 对象。`String` 对象是 *不可改变的（immutable）*，这意味着一旦创建，它们的值将不能改变。

[^1]: [boolean size not defined in java: why?](https://softwareengineering.stackexchange.com/questions/363286/boolean-size-not-defined-in-java-why)

#### 1.1.1 默认值（Default Values）

声明而没有初始化的 **字段（field）** 将由编译器设置一个合理的默认值。通常来说，这个值将是 0 或者 null，取决于数据类型。然而，**依赖于这样的默认值通常被认为是很糟糕的编程风格。**

| 数据类型                 | 默认值（对于字段）   |
| ------------------------ | -------------------- |
| byte                     | 0                    |
| short                    | 0                    |
| int                      | 0                    |
| long                     | 0L                   |
| float                    | 0.0f                 |
| double                   | 0.0d                 |
| char                     | '\u0000'             |
| String (or any object)   | null                 |
| boolean                  | FALSE                |

本地变量（local variable）略有不同； **编译器从不赋值一个默认值给一个未初始化的本地变量。** 访问未初始化的本地变量将导致编译时错误（compile-time error）。

#### 1.1.2 原义 / 字面量（Literals）

你或许注意到了，当初始化一个原始数据类型的变量时没有使用 `new` 关键字。原始类型是由语言中内置的数据类型，它们并不是从类中创建的对象。*字面量（literal）* 是一个固定值（fixed value）的源代码表示（source code representation）。字面量直接在代码中表示，无需计算（computation）。

##### 1.1.2.1 整数字面量（Integer Literals）

如果一个整数字面量以字面 `L` 或 `l` 结尾，那么它的类型为 `long`；否则它的类型为 `int`。推荐使用大写字母 `L`。

整数类型 `byte`、`short`、`int` 和 `long` 的 **值** 可以从 `int` 字面量中创建。可以从 `long` 字面量创建超出 `int` 范围的 `long` 类型的值。整形字面量可以用以下数字系统（number system）表达：

- 十进制
- 十六进制
- 二进制

前缀 `0x` 表示十六进制，`0b` 表示二进制。

##### 1.1.2.2 浮点字面量（Floating-point Literals）

如果以字母 `F` 或者 `f` 结尾，那么 这个浮点数字面量是 `float` 类型；否则它的类型为 `double`，可选的以字母 `D` 或者 `d` 结尾。浮点数类型（`float` 和 `double`）也可以使用 `E` 或者 `e`（科学计数法），`F` 或 `f`（32 位浮点（`float`）字面量）和 `D` 或者 `d`（64 位双精度（`double`）字面量，这个是默认和约定可以被省略）表示。

##### 1.1.2.3 字符和字符串字面量（Character and String Literals）

`char` 和 `String` 类型可以包含任何的 Unicode（UTF-16）字符。可以使用「Unicode 转义（Unicode escape）」，如 `'\u0108'`。**始终对 `char` 字面量使用「单引号（`'`）」，对 `String` 字面量使用「双引号（`"`）」。** Unicode 转义序列（escape sequenece）可以在程序的其它地方使用（例如，在字段名中），不仅仅在 `char` 或 `String` 字面量里。

Java 编程语言也支持一些特殊转义序列对于 `char` 和 `String` 字面量：

| 转义序列 | 作用（中） | 作用（英）      |
| -------- | ---------  | ---------       |
| `\b`     | 回退       | backspace       |
| `\t`     |            | tab             |
| `\n`     | 换行       | line feed       |
| `\f`     |            | form feed       |
| `\r`     |            | carriage return |
| `\"`     | 双引号     | double quote    |
| `\'`     | 单引号     | single quote    |

还有一个特殊的 `null` 字面量，可以作为任意引用类型（reference type）的值。`null` 可以赋值给任何变量， **但原始类型（primitive type）的变量除外**。除了测试它的存在，你几乎无法使用 `null` 值。因此，`null` 通常在程序中用作标记（marker），以告知（indicate）某些对象不可用（unaviabile）。

最后，还有一个字面量的特殊种类，叫做 *类字面量（class literal）*，通过输入类型名称（type name）并附加「`.class`」；例如，`String.class`，这个引用（`Class` 类型）的对象，代表类型本身。

#### 1.1.3 使用下划线字符（`_`）在数字字面量中

从 Java SE 7 和之后，任何数量的下划线字符（`_`）可以出现在数字字面量（numerical literal）中的数字之间的任何位置。

你仅可以放置下划线（`_`）在数字之间；你不可以放置下划线在以下位置：

- 数字的开头和结尾
- 与浮点字面量中的小数点相邻
- `F` 或 `f` 后缀之前
- In positions where a string of digits is expected

### 1.2 数组（Arrays）

*数组（array）* 是一个容器对象（container object），放置 **固定数量（fixed number）** 的 **单个类型** 值。在创建数组时，形成数组的长度（length）。在创建之后，它的长度被固定（fixed）。

![objects-tenElementArray](assets/pics/objects-tenElementArray.gif)

数组中的每个项（item）称为 *元素（element）*，每个元素可以由它的数字 *索引（index）* 访问。

#### 1.2.1 声明一个变量引用到一个数组（Declaring a Variable to Refer to an Array）

```java
// 声明一个整型数组
int[] anArray;
```

一个数组声明包含 2 个部分：数组类型和数组名称。数组类型以 `type[]` 这样写，这儿的 `type` 是容器中元素（contained element）的数据类型。方括号 `[]` 是特殊的符号，告知这个变量放置着数组。数组的大小（size）不是它类型的部分（这也是为什么方括号中是空的）。数组变量的名字可以是任何你想的，前提是它遵循前面 [命名](#naming) 部分讨论的规则和约定。正如其它类型的变量，声明（declaration）并 **不实际创建一个数组**；它简单地告诉编译器，这个变量将放置一个指定类型的数组。

你也可以把方括号（`[]`）放在数组名称后面：

```java
// 这个形式不被鼓励
float anArrayOfFloats[];
```

但是，约定不鼓励这种形式；方括号（`[]`）标识数组类型，并应该显示类型名称（type designation）。

#### 1.2.2 创建、初始化、和访问数组（Creating, Initializing, and Accessing an Array）

创建数组的一种方式是使用 `new` 操作符。

```java
// 创建一个整型数组
anArray = new int[10];
```

如果没有这个语句（初始化语句），编译器将打印像如下的一个错误，编译失败：

```
ArrayDemo.java:4: Variable anArray may not have been initialized.
```

接下来几行为数组中的每一个元素赋值：

```java
anArray[0] = 100; // initialize first element
anArray[1] = 200; // initialize second element
anArray[2] = 300; // and so forth
```

通过数组元素的数字索引（index）来访问它：

```java
System.out.println("Element 1 at index 0: " + anArray[0]);
System.out.println("Element 2 at index 1: " + anArray[1]);
System.out.println("Element 3 at index 2: " + anArray[2]);
```

或者，你也可以使用快捷语法（shortcut syntax）来创建和初始化数组：

```java
int[] anArray = { 
    100, 200, 300,
    400, 500, 600, 
    700, 800, 900, 1000
};
```

这儿，数组的长度（length）有花括号（`{}`）之间提供的逗号（`,`）分隔的值的数量确定。

你也可以创建一个数组的数组（array of arrays）（也被称作 *多维数组（multidimensional array）*），通过使用 2 组或多组方括号（`[]`），例如 `String[][]` 名。因此每个元素必须由 **相应数量的索引值** 来访问。

在 Java 编程语言中，多维数组是一个数组，它的部分本身就是数组。这不同与在 C 或 Fortran 中的数组。这带来一个结果是运行行的长度不同。

```java
class MultiDimArrayDemo {
    public static void main(String[] args) {
        String[][] names = {
            {"Mr. ", "Mrs. ", "Ms. "},
            {"Smith", "Jones"}
        };
        // Mr. Smith
        System.out.println(names[0][0] + names[1][0]);
        // Ms. Jones
        System.out.println(names[0][2] + names[1][1]);
    }
}
```
这个程序的输出结果是：

```
Mr. Smith
Ms. Jones
```

最后，你可以使用内置的 `length` 属性（property）来确定数组的大小（size）。

```java
System.out.println(anArray.length);
```

#### 1.2.3 复制数组（Copying Arrays）

`System` 类有一个 `arraycopy` 方法，你可以使用它有效地从一个数组到另一个复制数据。

```
public static void arraycopy(Object src, int srcPos,
                             Object dest, int destPos, int length)
```

#### 1.2.4 数组操作（Array Manipulations）

一些有用的操作由在 `java.util.Arrays` 类中的方法提供：

- Searching an array for a specific value to get the index at which it is placed (the binarySearch method).
- Comparing two arrays to determine if they are equal or not (the equals method).
- Filling an array to place a specific value at each index (the fill method).
- Sorting an array into ascending order. This can be done either sequentially, using the sort method, or concurrently, using the parallelSort method introduced in Java SE 8. Parallel sorting of large arrays on multiprocessor systems is faster than sequential array sorting.


## 操作符 / 运算符

操作符（operator）是对一个、两个或者三个 *操作数（operands）* 执行特定操作，并之后返回结果的特殊符号。

当我们探索 Java 编程语言的操作符时，提前知道哪个操作符有最高的优先级（precedence）对于你来说，或许是有帮助的。

1. 拥有**更高优先级** 的操作符在优先级相对较低的操作符之前 **先** 进行求值。
2. 当具有相同优先级的操作符在同一个表达式中时，必须有一个规则控制哪个优先被计算。
    + 除了赋值操作符之外的所有二元操作符都是 **从左到右** 进行计算的；赋值操作符（assignment operator）是从右到左进行求值的。

## 表达式、语句和语句块

#### 表达式（Expresions）

*表达式（expression）* 是由变量、操作符和方法调用（method invocations）组成的构造（construct），它根据语言的语法构造，计算为一个单值（single value）。

由表达式返回的值的数据类型依赖于在表达式中使用的元素。

#### 语句（Statements）

语句大致相当于自然语言的句子。一个 *语句（statement）* 形成了一个完整的执行单元。通过使用分号（`;`）终止表达式，可以将以下类型的表达式转换为语句。

- 赋值语句
- 任何使用 `++` 或者 `--`
- 方法调用
- 对象创建表达式

除了表达式语句，还有其它两种语句：*声明语句（declaration statement）* 和 *控制流语句（control flow statement）*。*声明语句* 是声明一个变量。*控制流语句* 调节（regulate）语句执行的顺序。

#### 语句块

*语句块（block）* 是花括号（`{}`）之间的一组零个或多个语句，可以在允许单个语句的任何位置使用。
